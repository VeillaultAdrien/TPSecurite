﻿<?php
include("connection.php");
session_start();
$login = @$_SESSION["login"];
$mdp = @$_SESSION["pass"];
$cnx = connection();
$requete = "SELECT * FROM utilisateur
WHERE login='$login' AND mdp='$mdp'";
$result = @mysqli_query($cnx,$requete);
$nb_ligne = @mysqli_num_rows($result);
$enr=mysqli_fetch_object($result) ;
if ($nb_ligne == 0) {
header("Location: connexion_prob.html");
return;
}
/* Utilisateur authentifié */
?>
<html>
	<head>
		<link rel="stylesheet" href="font-awesome-4.7.0\css\font-awesome.min.css">
		<link href="bootstrap-3.3.7-dist\css\bootstrap.min.css" rel="stylesheet">
		<link href="style.css" rel="stylesheet">
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<title>Modification des photographies</title>
	</head>
	<body>
<!----------------------------------------------------------Début Menu------------------------------------------------------------------------->	
	<nav class="navbar navbar-default navbar-fixed-top">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"></button>
		  <a class="navbar-brand" href="index.php"><i class="fa fa-home" aria-hidden="true"></i></a>
		</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="dropdown">
					  <a href="liste_clients.php?mode=client" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des clients <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="liste_clients.php?mode=client#lclients">Liste des clients</a></li>
						<li><a href="liste_clients.php?mode=client#aclient">Ajouter un client</a></li>
					  </ul>
					</li>
					<li class="dropdown">
					  <a href="liste_clients.php?mode=client" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des photographies <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="liste_photos.php?mode=photo#lphoto">Liste des photographies</a></li>
						<li><a href="liste_photos.php?mode=photo#aphoto">Ajouter une nouvelle photographie</a></li>
					  </ul>
					</li>
					<li class="dropdown">
					  <a href="liste_photos.php?mode=photo" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des achats <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="liste_achats.php?mode=achat#lachat">Liste des achats</a></li>
						<li><a href="liste_achats.php?mode=achat#a-achat">Ajouter un nouvel achat</a></li>
					  </ul>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<?php
						echo "<li><a href='profil.php'><i class='fa fa-user-o' aria-hidden='true'></i> $enr->login</a></li>" ;
					?>
					<li><a href="deconnexion.php"><i class="fa fa-window-close-o" aria-hidden="true"></i></a></li>
					  
				</ul>
		</div></div>
	</nav>
<!----------------------------------------------------------Fin Menu------------------------------------------------------------------------->	
<div class="container">
	<article>
		<section class="col-md-12 col_lg-12">
			<?php
					/*récupération des valeurs à afficher dans le formulaire*/
				$id_photo=@$_POST["id"] ;
				$requete = "select * from photo where (id='$id_photo');" ;
				$result = mysqli_query($cnx, $requete) ;
				if (!$result) {
					die ("<p>Requéte échouée</p>") ;} ;
				$enr=mysqli_fetch_object($result) ;
				$titre = $enr->Titre ;
				$auteur = $enr->auteur ;
				$date = $enr->dprise ;
				$resolution = $enr->resolution ;
				$couleur= $enr->couleur ;
				$lien_photo = $enr->lien_photo ;
				$id_photo = $enr->id ;
				$date_explode = explode('-',$date) ;
				echo "<h1>Modifier les informations de la photographie $titre</h1>" ;
					/*formulaire*/
				echo"<form method='post' action='modification.php' enctype='multipart/form-data' class='form-horizontal'>
					<div class='form-group'>
						<label for='titre' class='col-lg-2 col-md-2 control-label'>Titre : </label>
					<div class='col-lg-10 col-md-10'>
							<input type='text' name='titre' value='$titre' id='titre'/>
					</div></div>
					<div class='form-group'>
						<label for='auteur' class='col-lg-2 col-md-2 control-label'>Auteur : </label>
					<div class='col-lg-10 col-md-10'>
							<input type='text' name='auteur' value='$auteur' id='auteur'/>
					</div></div>
					<div class='form-group'>
						<label for='date' class='col-lg-2 col-md-2 control-label'>Date de prise de la photo :</label>
						<div class='col-lg-10 col-md-10'>
							<select name='jprise' id='date'>" ;
								$i=1 ;
								while ($i<32) {
									if ($i<10) {
										$z = 0 ;
									} else { $z = '' ;}
									if ($i == $date_explode[2]) {
										echo "<option  selected value='$z$i'>$i</option>" ;
									} else {
									echo "<option value='$z$i'>$i</option>" ; } ;
									$i++ ;
								} ;
				echo "</select> " ;
				$mois = array (
					'01' => 'Janvier',
					'02' =>'Février',
					'03' => 'Mars',
					'04' => 'Avril',
					'05' => 'Mai',
					'06' => 'Juin',
					'07' => 'Juillet',
					'08' => 'Août',
					'09' => 'Septembre',
					'10' => 'Octobre',
					'11' => 'Novembre',
					'12' => 'Décembre') ;
				echo "<select name='mprise'>" ;
				foreach($mois as $mois_chiffre => $mois_lettre) {
					if ($mois_chiffre == $date_explode[1])
					{
						echo "<option selected value='$mois_chiffre'>$mois_lettre</option>" ;
					} 
					else { echo "<option value='$mois_chiffre'>$mois_lettre</option>" ;}
				} ;
				echo "</select> " ;
				echo "<select name='aprise'>" ;
				$i=1900 ;
				$dateactuelle = getdate() ;
				while ($i <= $dateactuelle['year']) {
					if ($i == $date_explode[0])
					{
						echo "<option  selected value='$i'>$i</option>" ;
					} else 
					{
						echo "<option value='$i'>$i</option>" ; }
					$i++ ;
				};
				echo "</select>
					</div></div>
					<div class='form-group'>
						<label for='resolution' class='col-lg-2 col-md-2 control-label'>Résolution : </label>
					<div class='col-lg-10 col-md-10'>
						<input type='text' name='resolution' value='$resolution' id='resolution'/>
					</div></div>
					<div class='form-group'>
						<label for='couleur' class='col-lg-2 col-md-2 control-label'>Couleur : </label>" ;
				if ($couleur = '0') {echo "<div class='col-lg-10 col-md-10'>
					<select name='couleur' id='couleur'>
						<option selected value=0>Noir et blanc</option>
						<option value=1>Couleur</option>
					</select>
					</div></div>" ;}
				else { echo "<div class='col-lg-10 col-md-10'>
					<select name='couleur' id='couleur'>
						<option value=0>Noir et blanc</option>
						<option selected value=1>Couleur</option>
					</select></div></div>" ;} ;
					echo "<div class='form-group'>
						<div class='col-sm-offset-2 col-sm-10'>
							<label>
								<img src='$lien_photo' alt='photographie' height='150em'/></br>
								<input type='hidden' name='MAX_FILE_SIZE' value='512000'>
								<input type='file' name='img_photo' class='btn btn-default'/>
								<input type='hidden' name='action' value='modification_photo'>
							</label>
						<div class='form-group'>
						<div class='col-sm-offset-2 col-sm-10'>
						<label>
							<input type='hidden' name='id' value='$id_photo'/>
							<input type='submit' value='Modifier' class='btn btn-default'/>
						</label>
				</form>" ;
				mysqli_close($cnx) ;
			?>
		</section>
	</article>
	<footer class="text-center col-lg-12 col-md-12">
		<p>TP Securité </br></br>CSRF/XSS</p>
	</footer>
</div>
	<script src="bootstrap-3.3.7-dist/js/jquery.js"></script>
	<script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	</body>
</html>