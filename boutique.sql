-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  Dim 10 juin 2018 à 12:05
-- Version du serveur :  10.1.32-MariaDB
-- Version de PHP :  7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `boutique`
--

-- --------------------------------------------------------

--
-- Structure de la table `achat`
--

CREATE TABLE `achat` (
  `id_achat` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_photo` int(11) NOT NULL,
  `date` date NOT NULL,
  `prix` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `achat`
--

INSERT INTO `achat` (`id_achat`, `id_client`, `id_photo`, `date`, `prix`) VALUES
(9, 2, 8, '2010-05-18', 10),
(11, 2, 7, '2015-10-08', 50),
(12, 1, 10, '2016-10-28', 20),
(13, 22, 7, '1900-01-01', 30),
(14, 25, 10, '1900-01-01', 20),
(15, 24, 1, '1917-10-17', 40),
(16, 27, 10, '1900-01-01', 0);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `nom` text NOT NULL,
  `prenom` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id`, `nom`, `prenom`) VALUES
(26, 'Sanfo', 'Diane-Estelle'),
(24, 'Veillault', 'Adrien'),
(25, 'Tremblay', 'Jodie');

-- --------------------------------------------------------

--
-- Structure de la table `photo`
--

CREATE TABLE `photo` (
  `id` int(11) NOT NULL,
  `Titre` longtext,
  `auteur` longtext NOT NULL,
  `dprise` date NOT NULL,
  `resolution` text NOT NULL,
  `couleur` tinyint(1) NOT NULL,
  `lien_photo` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `photo`
--

INSERT INTO `photo` (`id`, `Titre`, `auteur`, `dprise`, `resolution`, `couleur`, `lien_photo`) VALUES
(1, 'Pigs in hat', 'Jean Guilloux', '2016-05-18', '300', 1, 'photographie/1478709885.png'),
(8, 'The dog and the cookie', 'Maurice Floral', '2008-12-18', '72', 1, 'photographie/1478710243.png'),
(7, 'Nausica', 'Hayao Miyazaki', '1971-11-07', '200', 1, 'photographie/1478705365.png'),
(9, 'Fish in red and white', 'Goby03', '1941-09-17', '72', 1, 'photographie/1478424879.png'),
(10, 'Chat_bocal', 'Maurice Fish', '2016-07-29', '72', 1, 'photographie/1479241894.png');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `login` text NOT NULL,
  `mdp` text CHARACTER SET utf8mb4 NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `login`, `mdp`, `email`) VALUES
(1, 'User1', '5f4dcc3b5aa765d61d8327deb882cf99', 'toto@toto.com'),
(2, 'test', '098f6bcd4621d373cade4e832627b4f6', ''),
(3, 'toto', '5d933eef19aee7da192608de61b6c23d', ''),
(4, 'Veillault_adrien', 'a3f175bb10dc206d7b3dfe8cb6864773', 'truc@truc.com');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `achat`
--
ALTER TABLE `achat`
  ADD PRIMARY KEY (`id_achat`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `achat`
--
ALTER TABLE `achat`
  MODIFY `id_achat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT pour la table `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
