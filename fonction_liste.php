<?php
	function liste($m, $cnx) { 
	
				/*affichage de la liste des clients et des options de modification et de suppression dans un unique tableau. Une ligne par client*/
			if ($m == "client") { 												
				$requete = "select * from client order by nom, prenom;" ;		
				$result = mysqli_query($cnx, $requete) ;
				if (!$result) {
					die ("<p>Requéte échouée</p>") ;} 
				echo "<div class='table-responsive'>
						<table class='table table-striped'>
							<colgroup>
							<col class='col-xs-2 col-md-2 col-lg-2'></col>
							<col class='col-xs-6 col-md-2 col-lg-2'></col>
							<col class='col-xs-4 col-md-2 col-lg-2'></col>
							</colgroup>	" ;
			while ($enr=mysqli_fetch_object($result)) 
				{$id_client = $enr->id ;
					echo "			<tr>
										<th><a href='achats_client.php?id=$id_client'>$enr->nom, $enr->prenom</a>
										</th>
										<td><form method='post' action='modif_client.php'>
											<input type='hidden' name='id' value='$id_client'/>
											<button type='submit' value='Modifier'/>
											<span class='fa fa-wrench' aria-hidden='true'></span></button></form>
										</td>
									</tr>
									
									
									" ; }
				echo "</table></div>" ;
			} 
				
				/*affichage de la liste des photographie et des options de modification et de suppression dans un unique tableau. Une tableau par photographie*/
			else if ($m == "photo") {											
				$requete = "select * from photo ;" ;							
				$result = mysqli_query($cnx, $requete) ;
				if (!$result) {
					die ("<p>Requéte échouée</p>") ;} 
			while ($enr=mysqli_fetch_object($result))
				{$id_photo = $enr->id ;
				$dateprise = explode("-",$enr->dprise) ;
				$couleur=($enr->couleur?'couleur' :'noir et blanc') ;
				echo "<div class='table-responsive'>
				<table class='table table-condensed'>
				    <colgroup>
					<col class='col-md-2 col-lg-2'></col>
					<col class='col-md-2 col-lg-2'></col>
					<col class='col-md-2 col-lg-2'></col>
					</colgroup>
							<tr>
								<th>Titre</th>
								<td>$enr->Titre</th>
								<th rowspan=5><img src='$enr->lien_photo' alt='$enr->Titre' class='img-responsive'/></td>
							</tr>
							<tr>
								<th>Auteur</th>
								<td>$enr->auteur</td>
							</tr>
							<tr>
								<th>Date de prise</th>
								<td>$dateprise[2]/$dateprise[1]/$dateprise[0]</td>
							</tr>
							<tr>
								<th>Résolution</th>
								<td>$enr->resolution dpi</td>
							</tr>
							<tr>
								<th>Colorimétrie</th>
								<td>$couleur</td>
							</tr>
							
				</table></div>" ;
				
			} }
			
				/*affichage de la liste des achats et des options de modification et de suppression. Chaque achat dans un tableau*/
			else if ($m == "achat") {										
				$requete = "select client.nom, client.prenom, photo.Titre, photo.auteur, photo.dprise, photo.resolution, photo.couleur, achat.id_achat, achat.date, achat.prix
							from achat, photo, client 
							where achat.id_photo=photo.id and achat.id_client=client.id ;" ;
				$result = mysqli_query($cnx, $requete) ;
				if (!$result) {
					die ("<p>Requéte échouée</p>") ;} 
			while ($enr=mysqli_fetch_object($result))
				{$dateprise = explode("-",$enr->dprise) ;
				$dateachat = explode("-",$enr->date) ;
				$couleur=($enr->couleur?'couleur' :'noir et blanc') ;
				$id_achat = $enr->id_achat ;
				echo "<div class='table-responsive'>
						<table class='table table-condensed'>
							<colgroup>
								<col class='col-xs-2 col-md-2 col-lg-2'></col>
								<col class='col-xs-6 col-md-6 col-lg-6'></col>
							</colgroup>		
								<tr>
									<th>Client</th>
									<td>$enr->nom $enr->prenom</td>
								</tr>
								<tr>
									<th>Date d'achat</th>
									<td>$dateachat[2]/$dateachat[1]/$dateachat[0]</td>
								</tr>
								<tr>
									<th>Prix</th>
									<td>$enr->prix €</td>
								</tr>
								<tr>
									<th>Titre de la photographie</th>
									<td>$enr->Titre</td>
								</tr>
								<tr>
									<th>Auteur</th>
									<td>$enr->auteur</td>
								</tr>
								<tr>
									<th>Date de prise de la photographie</th>
									<td>$dateprise[2]/$dateprise[1]/$dateprise[0]</td>
								</tr>
								<tr>
									<th>Résolution</th>
									<td>$enr->resolution</td>
								</tr>
								<tr>
									<th>Colorimétrie</th><td>$couleur</td>
								</tr>
								
						
							
						</table></div>" ;
				
 }
			}
	} 
?>