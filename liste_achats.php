<!DOCTYPE html>
<?php
include("connection.php");
session_start();
$login = @$_SESSION["login"];
$mdp = @$_SESSION["pass"];
$cnx = connection();
$requete = "SELECT * FROM utilisateur
WHERE login='$login' AND mdp='$mdp'";
$result = @mysqli_query($cnx,$requete);
$nb_ligne = @mysqli_num_rows($result);
$enr=mysqli_fetch_object($result) ;
if ($nb_ligne == 0) {
header("Location: connexion_prob.html");
return;
}
/* Utilisateur authentifié */
?>
<html>
	<head>
		<link rel="stylesheet" href="font-awesome-4.7.0\css\font-awesome.min.css">
		<link href="bootstrap-3.3.7-dist\css\bootstrap.min.css" rel="stylesheet">
		<link href="style.css" rel="stylesheet">
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<title>Gestion des achats</title>
	</head>
	<body>
<!----------------------------------------------------------Début Menu------------------------------------------------------------------------->	
	<nav class="navbar navbar-default navbar-fixed-top">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"></button>
		  <a class="navbar-brand" href="index.php"><i class="fa fa-home" aria-hidden="true"></i></a>
		</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="dropdown">
					  <a href="liste_clients.php?mode=client" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des clients <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="liste_clients.php?mode=client#lclients">Liste des clients</a></li>
						<li><a href="liste_clients.php?mode=client#aclient">Ajouter un client</a></li>
					  </ul>
					</li>
					<li class="dropdown">
					  <a href="liste_clients.php?mode=client" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des photographies <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="liste_photos.php?mode=photo#lphoto">Liste des photographies</a></li>
						<li><a href="liste_photos.php?mode=photo#aphoto">Ajouter une nouvelle photographie</a></li>
					  </ul>
					</li>
					<li class="dropdown">
					  <a href="liste_photos.php?mode=photo" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des achats <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="liste_achats.php?mode=achat#lachat">Liste des achats</a></li>
						<li><a href="liste_achats.php?mode=achat#a-achat">Ajouter un nouvel achat</a></li>
					  </ul>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<?php
						echo "<li><a href='profil.php'><i class='fa fa-user-o' aria-hidden='true'></i> $enr->login</a></li>" ;
					?>
					<li><a href="deconnexion.php"><i class="fa fa-window-close-o" aria-hidden="true"></i></a></li>
					  
				</ul>
		</div></div>
	</nav>
<!----------------------------------------------------------Fin Menu------------------------------------------------------------------------->	
<div class="container">	
	<article>
		<section class="col-lg-12 col-md-12">
			<h1 id="lachat">Liste des achats</h1>
			<?php
				include("fonction_liste.php") ;
				liste ($_GET["mode"], $cnx) ;
			?>
		</section>
		<section class="col-lg-12 col-md-12">
			<h1 id="a-achat">Rentrer un nouvel achat</h1>
			<form action="ajout.php" method="post" class="form-horizontal" >
				<div class="form-group">
					<label for="acheteur" class="col-lg-2 col-md-2 control-label">Acheteur :</label>
				<div class="col-lg-10 col-md-10">
					<select name="id_client" id="acheteur">
					<?php
						$requete1 = "SELECT * FROM client  ORDER BY nom;" ;
						$result1 = mysqli_query($cnx, $requete1) ;
						if (!$result1) {
							die ("<p>Requéte échouée</p>") ;} 
						while ($enr=mysqli_fetch_object($result1)) {
							echo "<option value='$enr->id'>$enr->nom $enr->prenom</option>" ;
						} 
					?>
					</select>
				</div></div>
				<div class="form-group">
					<label for="photo" class="col-lg-2 col-md-2 control-label">Photographie :</label>
				<div class="col-lg-10 col-md-10">
					<select name="photo" id="photo">
					<?php
						$requete2 = "SELECT id, titre FROM photo  ORDER BY titre;" ;
						$result2 = mysqli_query($cnx, $requete2) ;
						if (!$result2) {
							die ("<p>Requéte échouée</p>") ;} 
						while ($enr=mysqli_fetch_object($result2)) {
							echo "<option value='$enr->id'>$enr->titre</option>" ;
						} 
					?>
					</select>
				</div></div>
				<div class="form-group">
					<label for="prix" class="col-lg-2 col-md-2 control-label">Prix :</label>
				<div class="col-lg-10 col-md-10">
							<input type="number" name="prix" id="prix"/>
				</div></div>
				<div class="form-group">
					<label for="achat" class="col-lg-2 col-md-2 control-label">Date d'achat:</label> 
				<div class="col-lg-10 col-md-10">
					<select name="jachat" id="achat">
						<?php 
							$i=1 ;
							while ($i<32) {
								if ($i<10) {
									$z = 0 ;
								} else {$z ="" ;}
								echo "<option value='$z$i'>$i</option>" ;
								$i++ ;
							}
						?> 
					</select> 
					<select name="machat">
						<option value="01">Janvier</option> 
						<option value="02">Février</option> 
						<option value="03">Mars</option> 
						<option value="04">Avril</option> 
						<option value="05">Mai</option> 
						<option value="06">Juin</option>
						<option value="07">Juillet</option> 
						<option value="08">Août</option> 
						<option value="09">Septembre</option> 
						<option value="10">Octobre</option> 
						<option value="11">Novembre</option> 
						<option value="12">Décembre</option> 
					</select> 
					<select name="aachat">
						<?php
							$i=1900 ;
							$dateactuelle = getdate() ;
							while ($i <= $dateactuelle["year"]) {
								echo "<option value='$i'>$i</option>" ;
								$i++ ;
							}
						?>
					</select>
				</div></div>
				<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<label>
						<input type="hidden" name="action" value="achat"/>
						<input type="submit" name="valider" value="Ajouter" class="btn btn-default"/>
					</label>
				</div></div>	
			</form>
		</section>
	</article>
	<footer class="text-center col-lg-12 col-md-12">
		<p>TP Securité </br></br>CSRF/XSS</p>
	</footer>
</div>
	
	
	
	
	<script src="bootstrap-3.3.7-dist/js/jquery.js"></script>
	<script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	</body>
	<?php
	mysqli_close($cnx) ;
	?>
</html>