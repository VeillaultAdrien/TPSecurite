<!DOCTYPE html>
<?php
include("connection.php");
session_start();
$login = @$_SESSION["login"];
$mdp = @$_SESSION["pass"];
$cnx = connection();
$requete = "SELECT * FROM utilisateur
WHERE login='$login' AND mdp='$mdp'";
$result = @mysqli_query($cnx,$requete);
$nb_ligne = @mysqli_num_rows($result);
$enr=mysqli_fetch_object($result) ;
if ($nb_ligne == 0) {
header("Location: connexion_prob.html");
return;
}
/* Utilisateur authentifié */
?>
<html>
	<head>
		<link rel="stylesheet" href="font-awesome-4.7.0\css\font-awesome.min.css">
		<link href="bootstrap-3.3.7-dist\css\bootstrap.min.css" rel="stylesheet">
		<link href="style.css" rel="stylesheet">
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<title>Accueil</title>
	</head>
	<body>
<!----------------------------------------------------------Début Menu------------------------------------------------------------------------->	
	<div class="row">
	<nav class="navbar navbar-default navbar-fixed-top">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"></button>
		  <a class="navbar-brand" href="index.php"><span class="fa fa-home" aria-hidden="true"></span></a>
		</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="dropdown">
					  <a href="liste_clients.php?mode=client" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des clients <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="liste_clients.php?mode=client#lclients">Liste des clients</a></li>
						<li><a href="liste_clients.php?mode=client#aclient">Ajouter un client</a></li>
					  </ul>
					</li>
					<li class="dropdown">
					  <a href="liste_clients.php?mode=client" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des photographies <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="liste_photos.php?mode=photo#lphoto">Liste des photographies</a></li>
						<li><a href="liste_photos.php?mode=photo#aphoto">Ajouter une nouvelle photographie</a></li>
					  </ul>
					</li>
					<li class="dropdown">
					  <a href="liste_photos.php?mode=photo" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des achats <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="liste_achats.php?mode=achat#lachat">Liste des achats</a></li>
						<li><a href="liste_achats.php?mode=achat#a-achat">Ajouter un nouvel achat</a></li>
					  </ul>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<?php
						echo "<li><a href='profil.php'><span class='fa fa-user-o' aria-hidden='true'></span> $enr->login</a></li>" ;
					?>
					<li><a href="deconnexion.php"><span class="fa fa-window-close-o" aria-hidden="true"></span></a></li>
					  
				</ul>
		</div></div>
	</nav>
	</div>
<!----------------------------------------------------------Fin Menu------------------------------------------------------------------------->	
<div class="container">
	<article>
		<section class="col-lg-12 col-md-12">
			<h1 class="text-center">Accueil</h1>
				<p>Bienvenu sur le panneau d'adminsitration de la boutique de photographie Veilllault.</p>
				</p>
		</section>
	<article>
	</div>
	<footer class="text-center col-lg-12 col-md-12">
		<p>TP Securité </br></br>CSRF/XSS</p>
	</footer>
	
	<script src="bootstrap-3.3.7-dist/js/jquery.js"></script>
	<script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	</body>
	<?php
	mysqli_close($cnx) ;
	?>
</html>