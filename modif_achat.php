<!DOCTYPE html>
<?php
include("connection.php");
session_start();
$login = @$_SESSION["login"];
$mdp = @$_SESSION["pass"];
$cnx = connection();
$requete = "SELECT * FROM utilisateur
WHERE login='$login' AND mdp='$mdp'";
$result = @mysqli_query($cnx,$requete);
$nb_ligne = @mysqli_num_rows($result);
$enr=mysqli_fetch_object($result) ;
if ($nb_ligne == 0) {
header("Location: connexion_prob.html");
return;
}
/* Utilisateur authentifié */
?>
<html>
	<head>
		<link rel="stylesheet" href="font-awesome-4.7.0\css\font-awesome.min.css">
		<link href="bootstrap-3.3.7-dist\css\bootstrap.min.css" rel="stylesheet">
		<link href="style.css" rel="stylesheet">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Modification des achats</title>
	</head>
	<body>
<!----------------------------------------------------------Début Menu------------------------------------------------------------------------->		
	<nav class="navbar navbar-default navbar-fixed-top">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"></button>
		  <a class="navbar-brand" href="index.php"><i class="fa fa-home" aria-hidden="true"></i></a>
		</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="dropdown">
					  <a href="liste_clients.php?mode=client" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des clients <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="liste_clients.php?mode=client#lclients">Liste des clients</a></li>
						<li><a href="liste_clients.php?mode=client#aclient">Ajouter un client</a></li>
					  </ul>
					</li>
					<li class="dropdown">
					  <a href="liste_clients.php?mode=client" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des photographies <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="liste_photos.php?mode=photo#lphoto">Liste des photographies</a></li>
						<li><a href="liste_photos.php?mode=photo#aphoto">Ajouter une nouvelle photographie</a></li>
					  </ul>
					</li>
					<li class="dropdown">
					  <a href="liste_photos.php?mode=photo" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des achats <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="liste_achats.php?mode=achat#lachat">Liste des achats</a></li>
						<li><a href="liste_achats.php?mode=achat#a-achat">Ajouter un nouvel achat</a></li>
					  </ul>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<?php
						echo "<li><a href='profil.php'><i class='fa fa-user-o' aria-hidden='true'></i> $enr->login</a></li>" ;
					?>
					<li><a href="deconnexion.php"><i class="fa fa-window-close-o" aria-hidden="true"></i></a></li>
					  
				</ul>
		</div></div>
	</nav>
<!----------------------------------------------------------Fin Menu------------------------------------------------------------------------->	
<div class="container">
	<article>												
		<section class="col-lg-12 col-md-12">
				<?php
					/*récupération des valeurs à afficher dans le formulaire*/
				$id_achat =$_POST['id_achat'] ;
				$requete1 = "Select * from achat where (id_achat = $id_achat) ;" ;
				$result1 = mysqli_query($cnx, $requete1) ;
				$enr=mysqli_fetch_object($result1) ;
				$id_client = $enr->id_client ;
				$id_photo = $enr->id_photo ;
				$id_achat_bis = $enr->id_achat ;
				$date_achat = $enr->date ;
				$prix = $enr->prix ;
				$date_explode = explode('-',$date_achat) ;
				$requete_nom = "Select * from client where (id = $id_client) ;" ;
				$result_nom = mysqli_query($cnx, $requete_nom) ;
				$enr2=mysqli_fetch_object($result_nom) ;
				$nom = $enr2->nom ;
				$prenom = $enr2->prenom ;
				echo "<h1>Modification de l'achat du $date_explode[2]/$date_explode[1]/$date_explode[0] de $nom $prenom </h1>" ;
			?>
				<!-- Formulaire -->
			<form action='modification.php' method='post' class="form-horizontal">
				<div class="form-group">
					<label for='acheteur' class="col-lg-2 col-md-2 control-label">Acheteur :</label>
				<div class="col-lg-10 col-md-10">
					<select name='id_client' id='acheteur'> ;
					<?php
						$requete2 = "SELECT * FROM client  ORDER BY nom;" ;
						$result2 = mysqli_query($cnx, $requete2) ;
						if (!$result2) {
							die ("<p>Requéte échouée</p>") ;} 
						while ($enr=mysqli_fetch_object($result2)) {
							$id_client_bis = $enr->id ;
							if ($id_client == $id_client_bis) {
								echo "<option selected value='$enr->id'>$enr->nom $enr->prenom</option>" ;
							} else {
							echo "<option value='$enr->id'>$enr->nom $enr->prenom</option>" ; }
						} 
					?>
					</select>
				</div></div>
				<div class="form-group">
					<label for="photo" class="col-lg-2 col-md-2 control-label">Photographie :</label>
				<div class="col-lg-10 col-md-10">
					<select name="id_photo" id="photo">
					<?php
						$requete3 = "SELECT id, titre FROM photo  ORDER BY titre;" ;
						$result3 = mysqli_query($cnx, $requete3) ;
						if (!$result3) {
							die ("<p>Requéte échouée</p>") ;} 
						while ($enr=mysqli_fetch_object($result3)) {
							$id_photo_bis = $enr->id ;
							if ($id_photo == $id_photo_bis) {
								echo "<option selected value='$enr->id'>$enr->titre</option>" ;
							} else {
							echo "<option value='$enr->id'>$enr->titre</option>" ; }
						} 
					?>
					</select>
				</div></div>
				<div class="form-group">
					<label for="prix" class="col-lg-2 col-md-2 control-label">Prix :</label>
				<div class="col-lg-10 col-md-10">
					<?php
					 echo "<input type='float' name='prix' id='prix' value ='$prix'/></br>" ;
					?>
				</div></div>
				<div class="form-group">
					<label for="achat" class="col-lg-2 col-md-2 control-label">Date d'achat:</label> 
				<div class="col-lg-10 col-md-10">
					<select name="jachat" id="achat">
						<?php 
							$i=1 ;
							while ($i<32) {
								if ($i<10) {
									$z = 0 ;
								} else { $z = '' ;}
								if ($i == $date_explode[2]) {
									echo "<option  selected value='$z$i'>$i</option>" ;
								} else {
								echo "<option value='$z$i'>$i</option>" ; } ;
								$i++ ;
							} ;
						?> 
					</select> 
					<?php 
					$mois = array (
						'01' => 'Janvier',
						'02' =>'Février',
						'03' => 'Mars',
						'04' => 'Avril',
						'05' => 'Mai',
						'06' => 'Juin',
						'07' => 'Juillet',
						'08' => 'Août',
						'09' => 'Septembre',
						'10' => 'Octobre',
						'11' => 'Novembre',
						'12' => 'Décembre') ;
					echo "<select name='machat' id='achat'>" ;
					foreach($mois as $mois_chiffre => $mois_lettre) {
						if ($mois_chiffre == $date_explode[1])
							{
								echo "<option selected value='$mois_chiffre'>$mois_lettre</option>" ;
							} else { 
									echo "<option value='$mois_chiffre'>$mois_lettre</option>" ;
					}
					} ;
					echo "</select> " ;
					?>
					<select name="aachat" id="achat">
						<?php
							$i=1900 ;
							$dateactuelle = getdate() ;
							while ($i <= $dateactuelle['year']) {
								if ($i == $date_explode[0])
								{
									echo "<option  selected value='$i'>$i</option>" ;
								} else {
								echo "<option value='$i'>$i</option>" ; }
							$i++ ; }
						?>
					</select>
				</div></div>
					<?php
					echo "<input type='hidden' name='id_achat' value='$id_achat_bis'/>";
					?>
				<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<label>
						<input type="hidden" name="action" value="modification_achat"/>
						<input type="submit" name="Modifier" value="Modifier" class="btn btn-default"/>
					</label>
				</div></div>	
			</form>
		</section>
	</article>
	<footer class="text-center col-lg-12 col-md-12">
		<p>TP Securité </br></br>CSRF/XSS</p>
	</footer>	
	
</div>
	
	
	
	<script src="bootstrap-3.3.7-dist/js/jquery.js"></script>
	<script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	</body>
	<?php
	mysqli_close($cnx) ;
	?>
</html>