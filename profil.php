﻿<?php
include("connection.php");
session_start();
$login = @$_SESSION["login"];
$mdp = @$_SESSION["pass"];
$cnx = connection();
$requete = "SELECT * FROM utilisateur
WHERE login='$login' AND mdp='$mdp'";
$result = @mysqli_query($cnx,$requete);
$nb_ligne = @mysqli_num_rows($result);
$enr=mysqli_fetch_object($result) ;
if ($nb_ligne == 0) {
header("Location: connexion_prob.html");
return;
}
/* Utilisateur authentifié */
?>
<html>
	<head>
		<link rel="stylesheet" href="font-awesome-4.7.0\css\font-awesome.min.css">
		<link href="bootstrap-3.3.7-dist\css\bootstrap.min.css" rel="stylesheet">
		<link href="style.css" rel="stylesheet">
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<title>Profil</title>
	</head>
	<body>
<!----------------------------------------------------------Début Menu------------------------------------------------------------------------->	
	<nav class="navbar navbar-default navbar-fixed-top">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"></button>
		  <a class="navbar-brand" href="index.php"><i class="fa fa-home" aria-hidden="true"></i></a>
		</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="dropdown">
					  <a href="liste_clients.php?mode=client" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des clients <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="liste_clients.php?mode=client#lclients">Liste des clients</a></li>
						<li><a href="liste_clients.php?mode=client#aclient">Ajouter un client</a></li>
					  </ul>
					</li>
					<li class="dropdown">
					  <a href="liste_clients.php?mode=client" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des photographies <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="liste_photos.php?mode=photo#lphoto">Liste des photographies</a></li>
						<li><a href="liste_photos.php?mode=photo#aphoto">Ajouter une nouvelle photographie</a></li>
					  </ul>
					</li>
					<li class="dropdown">
					  <a href="liste_photos.php?mode=photo" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion des achats <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="liste_achats.php?mode=achat#lachat">Liste des achats</a></li>
						<li><a href="liste_achats.php?mode=achat#a-achat">Ajouter un nouvel achat</a></li>
					  </ul>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<?php
						echo "<li><a href='profil.php'><i class='fa fa-user-o' aria-hidden='true'></i> $enr->login</a></li>" ;
					?>
					<li><a href="deconnexion.php"><i class="fa fa-window-close-o" aria-hidden="true"></i></a></li>
					  
				</ul>
		</div></div>
	</nav>
<!----------------------------------------------------------Fin Menu------------------------------------------------------------------------->	
<div class="container">
	<article>												
		<section class="col-md-12 col_lg-12">
			<?php
				$id_user = $enr->id ;
				$user = $enr->login ;
				$email = $enr->email ;
				
				/*Affiche des données utilisateur*/
				echo "
					<h1>Profil de $user</h1>
					<table>
							<tr>
								<th>Nom d'utilisateur :</th>
								<td>$user</td>
							</tr>
							<tr>
								<th>E-mail :</th>
								<td>$email</td>
							</tr>
							<tr>
							<td></td>
							<td><form method='post' action='modification_profil.php'>
									<input type='submit' name='action' value='Modifier' class='btn btn-default'/>
								</form>
							</td>
					</table>
				" ;
				mysqli_close($cnx) ;
			?>
		</section>
	</article>
	
	<footer class="text-center col-lg-12 col-md-12">
		<p>TP Securité </br></br>CSRF/XSS</p>
	</footer>
	
	<script src="bootstrap-3.3.7-dist/js/jquery.js"></script>
	<script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	</body>
</html>